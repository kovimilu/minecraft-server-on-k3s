# Minecraft Server on Kubernetes

This repository contains Kubernetes manifests for deploying a Minecraft server within a Kubernetes cluster. The deployment includes a sidecar container for monitoring the server's status, though this is optional and commented out by default.

## Overview

The setup includes:

- A Deployment for the Minecraft server, configured with resource requests and limits to ensure optimal performance.
- A PersistentVolumeClaim (PVC) and PersistentVolume (PV) for game data persistence.
- Services for the Minecraft server and (optionally) metrics collection through a monitoring sidecar.

## Prerequisites

- A Kubernetes cluster
- kubectl configured to communicate with your cluster
- Basic knowledge of Kubernetes resource definitions

## Deployment

### Deploying the Minecraft Server

1. **Persistent Volume and Claim**: First, set up persistent storage by applying the PV and PVC manifests. This ensures game data is preserved across pod restarts and redeployments.

    ```sh
    kubectl apply -f persistent-volume.yaml
    kubectl apply -f persistent-volume-claim.yaml
    ```

2. **Minecraft Server Deployment**: Deploy the Minecraft server using its deployment manifest. This will also mount the previously created PVC for game data storage.

    ```sh
    kubectl apply -f minecraft-server-deployment.yaml
    ```

3. **Services**: Apply the service definitions to expose the Minecraft server and, optionally, the monitoring service.

    ```sh
    kubectl apply -f minecraft-service.yaml
    # Optional: If using monitoring
    kubectl apply -f minecraft-metrics-service.yaml
    ```

### Configuration

- **Server Memory**: The `MAX_MEMORY` environment variable can be adjusted based on available resources.
- **Volume Storage Path**: Modify the storage path in the PV definition according to your needs.

## Monitoring (Optional)

The setup includes an optional monitoring sidecar (`mc-monitor`) that can be enabled to collect metrics from the Minecraft server. To enable it:

1. Uncomment the `mc-monitor` container definition in the deployment manifest.
2. Apply the changes to the deployment.

## Accessing the Server

The Minecraft server can be accessed at the external IP assigned to the `minecraft-service` LoadBalancer service on the standard Minecraft port (25565).

## Contributing

Contributions to improve the deployment or add new features are welcome. Please submit a pull request or open an issue to discuss proposed changes.

## License

This project is open source and available under the [MIT License](LICENSE.md).

## Acknowledgements

- Thanks to the maintainers of the `itzg/minecraft-server` Docker image for providing an easy-to-use Minecraft server setup.
- Thanks to the Kubernetes community for the extensive documentation and support resources.
